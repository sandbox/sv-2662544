(function ($) {
  Drupal.behaviors.usMap = {
    attach: function (context, settings) {

      var styles = {};
      var textStyles = {};
      var hoverStyles = {};
      var clickHandlers = {};

      var impacted = [];
      var nonState = [];
      var noImpacted = [];

      var states = Drupal.settings.states;

      if (typeof states === 'object') {
        $.each(states, function (index, value) {
          styles[index] = {};
          styles[index].fill = '#' + value.color;

          if (value.color == 'ffffff'){
            textStyles[index] = {};
            textStyles[index].fill = '#242424';
          }

          if (typeof value.url != 'undefined' && value.url.length > 1) {

            hoverStyles[index] = {};
            hoverStyles[index].fill = '#' + value.rollover_color;

            clickHandlers[index] = {};
            clickHandlers[index].url = value.url;

          }

          switch(value.status) {
            case 0:
              nonState.push(value.link);
              break;
            case 1:
              impacted.push(value.link);
              break;
            case 2:
              noImpacted.push(value.link);
              break;
          }
        });
      }

      var us_map_id = $('#us_map');

      us_map_id.usmap({
        'stateStyles': {
          fill: "#fff",
          stroke: "#000",
          "stroke-width": 1,
          "stroke-linejoin": "round",
          scale: [1, 1]
        },

        'labelBackingHoverStyles': {
          fill: "#fff",
          stroke: "#000"
        },

        stateSpecificLabelBackingStyles: styles,
        stateSpecificLabelTextStyles: textStyles,
        stateSpecificLabelBackingHoverStyles: hoverStyles,

        stateHoverStyles: false,
        stateSpecificStyles: styles,
        stateSpecificHoverStyles: hoverStyles,

        click: function (event, data) {

          if (typeof clickHandlers[data.name] != 'undefined') {

            var url = clickHandlers[data.name].url;

            if (url.length > 1) {
              var redirect = checkhttp(url);
              window.location = redirect;
            }
          }
        }
      });

      var nonStateTitle = Drupal.t('Non States');
      var impactedTitle = Drupal.t('Subsidies Impacted');
      var noImpactedTitle = Drupal.t('Subsidies Not Impacted');

      us_map_id.after(
        '<div class="legend"><p class="none-state"><span></span>'
        + nonStateTitle
        + '</p><p class="state-affected"><span></span>'
        + impactedTitle
        + '</p><p class="state-not-affected"><span></span>'
        + noImpactedTitle
        + '</p></div>'
      );

      $('#list-state').html(
        '<div class="legendMobile"><p class="none-state">'
        + '<h5>' + nonStateTitle + '</h5>'
        + nonState.join(' | ')
        + '</p><p class="state-affected">'
        + '<h5>' + impactedTitle + '</h5>'
        + impacted.join(' | ')
        + '</p><p class="state-not-affected">'
        + '<h5>' + noImpactedTitle + '</h5>'
        + noImpacted.join(' | ')
        + '</p></div>'
      );
    }
  }

  function checkhttp(url) {
    var redirect = '';

    if (/^(f|ht)tps?:\/\//i.test(url)) {
      redirect = url;
    }
    else {
      redirect = Drupal.settings.basePath + url;
    }

    return redirect;
  }

})(jQuery);
<?php

/**
 * @file
 * Admin "US Map" page.
 */

/**
 * Settings form for "US Map".
 */
function us_map_admin_settings_form($form, &$form_state) {

  $form = array(
    'table' => array(
      '#theme' => 'us_map_form_table',
      '#header' => array(t('States'), t('Url'), t('State Status')),
      'us_map_rows' => array(
        '#tree' => TRUE,
      ),
    ),
  );

  $states = us_map_get_states();
  $opt = variable_get('us_map_rows');

  foreach ($states as $index => $state) {
    //Get color of state by status
    switch ($opt[$index]['status']) {
      case '0':
        $color = '#000000';
        break;
      case '1':
        $color = '#A75C9A';
        break;
      case '2':
        $color = '#2281AD';
        break;
    }
    $form['table']['us_map_rows'][$index] = array(
      'title' => array(
        '#type' => 'item',
        '#markup' => "$state ($index)",
      ),
      'url' => array(
        '#type' => 'textfield',
        '#size' => 60,
        '#default_value' => isset($opt[$index]['url']) ? $opt[$index]['url'] : '',
      ),
      'status' => array(
        '#type' => 'select',
        '#options' => array(
          0 => t('Non States'),
          1 => t('Subsidies Impacted'),
          2 => t('Subsidies Not Impacted'),
        ),
        '#default_value' => !empty($opt[$index]['status']) ? $opt[$index]['status'] : 0,
        '#attributes' => array(
          'style' => 'color: ' . $color
        )
      ),
    );
  }

  return system_settings_form($form);
}
